---
title: "User directories in nginx"
date: 2021-11-01
draft: false
tags:
  - server
  - config
---

I run my own website, on a small rented vps.
One part involved in hosting your own website is the choice of webserver.
I decided to go with [nginx](https://nginx.org), a very popular one.
After installing, I tried to figure out how to set up user directories.
This means that going to `leaft.ch/~nzure` would serve the content in
`/home/nzure/www`, thus allowing every user to have their own website.

However, things didn't go as smooth as expected.
Nginx kept complaining about permissions and files, throwing around 403's and
404's.
And, as always, the internet had no answer.
I managed to fix the problem eventually, so im here to present the solution.

First, add your server block to nginx.
It should contain something like the code block below.
The only magic here is the regex, which matches the urls mentioned above.

```nginx
server {
	server_name domain.tld;
	root /root;

	index index.html;

	location ~ ^/~(.+?)(/.*)?$ {
		alias /home/$1/www$2;
	}
}
```

Next up is the most important part, file permissions.
Your `home` directory should be <q>711</q> (or <q>drwxr-xr-x</q>),
and `~/www` should be <q>755</q>.
This allows nginx access to all the relevant files.

That's it, it should work now.
I still don't think that this is the ideal solution, but I couldn't find any
better information on this topic.
Seems like not many people are interested in user directories, and more focused
on proxies and gzipping.

Until next time!
